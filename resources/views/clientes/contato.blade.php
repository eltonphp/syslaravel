@extends('layout.template')


@section('content')
<div class="container">
 <div class="row">
 <div class="col-md-4">
 <form action="{{ url('send') }}" method="post" enctype="multipart/form-data">
 {{ csrf_field() }}
  <div class="form-group">
    <label for="email">Email:</label>
    <input type="email" class="form-control" id="email" name="email">
  </div>
  <div class="form-group">
    <textarea type="text" name="msg" cols="42" rows="10"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Enviar</button>
</form> 
</div>
</div>
</div>


@stop