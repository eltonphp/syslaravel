@extends('layout.template')

@section('content')
<div>
  <form method="POST" action="{{action('ClienteController@update', $id)}}">
   {{ csrf_field() }}
   <input name="_method" type="hidden" value="PUT">
  <div class="form-group">
    <label for="email">Nome:</label>
    <input type="text" class="form-control" id="nome" name="nome" value="{{ $cliente->nome }}">
  </div>
  <div class="form-group">
    <label for="text">E-mail:</label>
    <input type="email" class="form-control" id="email" name="email" value="{{ $cliente->email }}">
  </div>
  <div class="form-group">
    <label for="pwd">Telefone:</label>
    <input type="text" class="form-control" id="telefone" name="telefone" value="{{ $cliente->telefone }}">
  </div>
  <button type="submit" class="btn btn-default">Cadastrar</button>
 </form>
</div>
@stop