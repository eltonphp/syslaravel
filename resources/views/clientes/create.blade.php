@extends('layout.template')

@section('content')
<div>
  <form action="{{ url('crud') }}" method="POST">
   {{ csrf_field() }}
  <div class="form-group">
    <label for="email">Nome:</label>
    <input type="text" class="form-control" id="nome" name="nome">
  </div>
  <div class="form-group">
    <label for="text">E-mail:</label>
    <input type="email" class="form-control" id="email" name="email">
  </div>
  <div class="form-group">
    <label for="pwd">Telefone:</label>
    <input type="text" class="form-control" id="telefone" name="telefone">
  </div>
  <button type="submit" class="btn btn-default">Cadastrar</button>
 </form>
</div>
@stop