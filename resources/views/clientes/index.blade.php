@extends('layout.template')

@section('content')
<a href="{{ url('crud/create') }}" class="btn btn-primary">Cadastrar novo cliente</a> |
<a href="{{ url('/contato') }}" class="btn btn-success">Enviar e-mail</a>
<table class="table table-striped">
	<thead>
		<th>Nome</th>
		<th>E-mail</th>
		<th>Telefone</th>
		<th>Editar</th>
		<th>Apagar</th>
	</thead>
	<tbody>
	    @forelse($clientes as $cliente)
		<tr>
			<td><a href="{{ action('ClienteController@show', $cliente->id ) }}">{{ $cliente->nome }}</a></td>
			<td>{{ $cliente->email }}</td>
			<td>{{ $cliente->telefone }}</td>
			<td><a href="{{ action('ClienteController@edit', $cliente->id) }}">editar</a></td>
			<td>
			 <form action="{{action('ClienteController@destroy', $cliente->id) }}" method="post">
	            {{csrf_field()}}
	            <input name="_method" type="hidden" value="DELETE">
	            <button class="btn btn-danger" type="submit">Delete</button>
            </form>			  
			</td>
		</tr>
		@empty
		<h2>Nenhum registro encontrado</h2>
		@endforelse
	</tbody>
</table>
{{ $clientes->links() }}
@stop