<?php

namespace clientela\Http\Controllers;

use Illuminate\Http\Request;

class ContatoController extends Controller
{
    public function contato()
    {
    	return view('clientes.contato');
    }

    public function send_email(Request $request)
    {
    	$email = $request->email;
    	$mensagem = $request->msg;

    	$dados = array('email'=>$email, 'msg'=>$mensagem);

    	Mail::send('clientes.email', $data, function())
    }
}
