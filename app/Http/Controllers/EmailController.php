<?php

namespace clientela\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
	public $title;

    public function contato()
    {
    	return view('clientes.contato');
    }


    public function send(Request $request)
    {

    	$title   = $request->input('email');
    	$content = $request->input('msg');

    	Mail::send('clientes.send', ['title'=>$title, 'content'=>$content], function($message)
    	{

           $message->subject('Laravel e-mail');
           $message->from('eltondeveloperweb@gmail.com','Elton Adriano');
           $message->to('eltonajs@gmail.com');

    	});

    	//return redirect('/crud');

    	return response()->json(['message'=>'Request completed']);
    }
}
