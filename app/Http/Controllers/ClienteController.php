<?php

namespace clientela\Http\Controllers;

use Illuminate\Http\Request;
use clientela\Http\Models\Cliente;
use Illuminate\Support\Facades\Mail;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if($id == null){

            $clientes = Cliente::paginate(3);
            return view('clientes.index', compact('clientes'));

        }else{

            return $this->show($id);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = new Cliente;

        $cliente->nome     = $request->get('nome');
        $cliente->email    = $request->get('email');
        $cliente->telefone = $request->get('telefone');
        $teste = $cliente->save();

        if($teste){
            Mail::send('clientes.send', ['title'=>$cliente->nome, 'content'=>$cliente->email], function($message)
            {

               $message->subject('Laravel e-mail');
               $message->from('eltondeveloperweb@gmail.com','Elton Adriano');
               $message->to('eltonajs@gmail.com');

            });
        }

        return redirect('/crud');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('clientes.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);

        return view('clientes.edit', compact('cliente','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::findOrFail($id);

        $cliente->nome     = $request->get('nome');
        $cliente->email    = $request->get('email');
        $cliente->telefone = $request->get('telefone');
        $cliente->save();

        return redirect('/crud');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $apagar = Cliente::find($id);
        $apagar->delete();

        return redirect('/crud');
    }

}
